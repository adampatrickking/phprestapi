<?php

mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_language('uni');
mb_regex_encoding('UTF-8');

/* ====================================================================================== */
/* 									Abstract Class                                        */
/* ====================================================================================== */

abstract class API
{	
	/*
	 * The HTTP method (GET, POST, PUT, DELETE)
	 */
	protected $method = '';

	/*
	 * The model requested (E.g.: /users)
	 */
	protected $endpoint = '';

	/*
	 * Descriptor for handling endpoint manipulation
	 */
	protected $verb = '';

	/* 
	 * Arguments remaining after the endpoint and verbs have been
	 * stripped
	 * E.g: /<endpoint>/<verb>/<arg0>/<arg1>
	 *   or /<endpoint>/<arg0>
	 */
	protected $args = array();

	/*
	 * Stores any contents of files submitted by PUT methods
	 */
	protected $file = Null;

	public function __construct($request) {

		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Methods: *");
		header("Content-Type: application/json");

		// Explode the arguments into the args array and 
		// trim the slashes
		$this->args = explode('/', rtrim($request, '/'));

		// Remove the first element of args and return it to $endpoint
		$this->endpoint = array_shift($this->args);

		// If there are still arguments remaining shift the array and
		// return the element to the $verb field
		if (array_key_exists(0, $this->args)) {
			$this->verb = array_shift($this->args);
		}

		// Get the method requested from the $_SERVER global variable
		$this->method = $_SERVER['REQUEST_METHOD'];

		// If the method type is POST get the HTTP method header type
		if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
			if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
				$this->method = 'DELETE';
			} else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
				$this->method = 'PUT';
			} else {
				throw new Exception("Unexpected Header");
			}
		}


		switch($this->method) {
			case 'DELETE':
			case 'POST':
				$this->request = $this->_cleanInputs($_POST);
				break;
			case 'GET':
				$this->request = $this->_cleanInputs($_GET);
				break;
			case 'PUT':
				$this->request = $this->_cleanInputs($_GET);
				$this->file = file_get_contents("php://input");				
				break;
			default:
				$this->_response('Invalid Method', 405);
				break;
		}
	} // end constructor

	public function processAPI() {
		if (method_exists($this, $this->endpoint)) {
			return $this->_response($this->{$this->endpoint}($this->args));
		}
		return $this->_response("No Endpoint: $this->endpoint", 404);
	}

	private function _response($data, $status = 200) {
		header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
		return json_encode($data);
	}

	private function _cleanInputs($data) {
		$clean_input = Array();
		if (is_array($data)) {
			foreach ($data as $k => $v) {
				$clean_input[$k] = $this->_cleanInputs($v);
			}
		} else {
			$clean_input = trim(strip_tags($data));
		}
		return $clean_input;
	}

	private function _requestStatus($code) {
		$status = array(
			200 => 'OK',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			500 => 'Internal Server Error'
		);
    	return ($status[$code]) ? $status[$code] : $status[500]; 
	}
}

/* ====================================================================================== */
/* 									Concrete Class                                        */
/* ====================================================================================== */


class MyAPI extends API
{
	protected $data;

	public function __construct($request, $origin) {
		parent::__construct($request);

		$this->argQuery = "";

		foreach ($this->args as $key => $value) {
			$this->argQuery = $this->argQuery . " AND " . $value . " = " . $this->verb;
		}

		$this->debug = array(
			'request' => $request,
			'origin' => $origin,
			'method' => $this->method,
			'args' => $this->args,
			'endpoint' => $this->endpoint,
			'verb' => $this->verb,
			'file' => $this->file,
			'argQuery' => $this->argQuery,
			$this->method => implode(",", $this->args)
		);
	}

	protected function debug() {
		if ($this->method == 'GET') {
			return json_encode($this->debug);
		}
	}

	protected function user() {
		if ($this->method == 'GET') {
			
			// Do MySql

		}
	}

	protected function panel() {
		if ($this->method == 'GET') {

			$return = array();
			
			// TODO:- Turn all SQL queries into parameter-based calls.
			
			$dsn = "mysql:dbname=LithiumPanel;host=backup.modrealms.net;charset=utf8";
			$dun = "LithiumPanel";
			// Not actually the password, changed for demonstration code.
			$dpw = "lithiumpassword";

			$query;
			$countQuery;
			$count;

			if ($this->verb === '') {
				$query = "SELECT * FROM clients";
				$countQuery = "SELECT COUNT(*) FROM clients";
			} else {
				$select;
				if (count($this->args) > 0) {
					$select = implode(",", $this->args);
				} else {
					$select = "*";
				}
				$query = "SELECT " . $select . " FROM services WHERE name = '" . $this->verb . "'";
				$countQuery = "SELECT COUNT(*) FROM services WHERE name = '" . $this->verb . "'";
			}

			try {
				$dbh = new PDO($dsn, $dun, $dpw);
				$count = $dbh->query($countQuery)->fetchColumn();
				$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			} catch (PDOException $e) {
				echo "Connection Failed: " . $e->getMessage();
				die();
			}
			
			try {
				foreach($dbh->query($query) as $row) {
					$temp = array();

					if ($count > 1) {
						foreach($row as $key => $value) {
							$temp[$key] = $value;
						}

						array_push($return, $row);
					} else {
						$return = $row;
						break;
					}
				}
			} catch (PDOException $e) {
				if ($e->getCode() === "42S22") {
					return "Query Failed: The field you are looking for does not exist.";
				} else {
					return "Query Failed: " . $e->getMessage();
				}
				die();
			}

			if (count($return) > 0) {
				return json_encode($return);
			} else {
				return json_encode("Error: Missing Data");
			}
		}
	}
}

?>